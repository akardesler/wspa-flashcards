/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.hkardesler.wspaflashcards.model.CollectionModel;
import com.hkardesler.wspaflashcards.model.FlashcardModel;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = DBHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "com.hkardesler.wspaflashcards.database";
    private static final String TABLE_COLLECTION = "tbl_collection";
    private static final String TABLE_FLASHCARDS = "tbl_flashcards";
    public static final String KEY_ID = "_id";
    public static final String KEY_COLLECTION_NAME = "collectionName";
    public static final String KEY_FRONT_TEXT = "frontText";
    public static final String KEY_BACK_TEXT = "backText";
    public static final String KEY_FRONT_IMAGE_ID = "frontImageId";
    public static final String KEY_BACK_IMAGE_ID = "backImageId";
    public static final String KEY_COLOR = "color";
    public static final String KEY_PATTERN = "pattern";
    public static final String KEY_COLLECTION_ID = "collectionId";
    public static DBHelper databaseHelper;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        databaseHelper = this;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_COLLECTION_TABLE = "CREATE TABLE " + TABLE_COLLECTION + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_COLLECTION_NAME + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_COLLECTION_TABLE);

        String CREATE_FLASHCARDS_TABLE = "CREATE TABLE " + TABLE_FLASHCARDS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_FRONT_TEXT + " TEXT,"
                + KEY_BACK_TEXT + " TEXT,"
                + KEY_FRONT_IMAGE_ID + " TEXT,"
                + KEY_BACK_IMAGE_ID + " TEXT,"
                + KEY_COLOR + " TEXT,"
                + KEY_PATTERN + " TEXT,"
                + KEY_COLLECTION_ID + " INTEGER" + ")";
        sqLiteDatabase.execSQL(CREATE_FLASHCARDS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_COLLECTION);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_FLASHCARDS);
        onCreate(sqLiteDatabase);
    }

    public void addCollection(String collectionName) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_COLLECTION_NAME, collectionName);
        db.insert(TABLE_COLLECTION, null, values);
    }

    public void updateCollection(int _id, String collectionName) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            db.execSQL("UPDATE " + TABLE_COLLECTION +
                    " SET collectionName ='" + collectionName + "' WHERE _id = " + _id);
            db.setTransactionSuccessful();
        } finally {
            if(db.inTransaction()){
                db.endTransaction();
            }
        }
    }

    public ArrayList<CollectionModel> getCollections() {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        ArrayList<CollectionModel> collections = new ArrayList<>();

        String QUERY = "SELECT * FROM " + TABLE_COLLECTION + " ORDER BY _id DESC";
        Cursor cursor = db.rawQuery(QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    int collectionID = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    String collectionName = cursor.getString(cursor.getColumnIndex(KEY_COLLECTION_NAME));
                    CollectionModel itemObject = new CollectionModel(collectionName);
                    itemObject.setFlashCardCount(getFlashcardsCount(collectionID));
                    itemObject.setId(collectionID);
                    collections.add(itemObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "error");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return collections;
    }

    public long getFlashcardsCount(int collectionId){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String whereClause = "collectionId = "+collectionId;
        long count = DatabaseUtils.queryNumEntries(db, TABLE_FLASHCARDS, whereClause);
        db.close();
        return count;
    }

    public void deleteCollection(int _id) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            db.execSQL("DELETE from " + TABLE_COLLECTION + " WHERE _id =" + _id );
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.d(TAG, "Error while trying to delete  users detail");
        } finally {
            if(db.inTransaction()){
                db.endTransaction();
            }
        }
    }

    public void addFlashcards(FlashcardModel card) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FRONT_TEXT, card.getFrontText());
        values.put(KEY_BACK_TEXT, card.getBackText());
        values.put(KEY_FRONT_IMAGE_ID, card.getFrontImageId());
        values.put(KEY_BACK_IMAGE_ID, card.getBackImageId());
        values.put(KEY_COLOR, card.getFrontColor());
        values.put(KEY_PATTERN, card.getFrontPattern());
        values.put(KEY_COLLECTION_ID, card.getCollectionId());
        db.insert(TABLE_FLASHCARDS, null, values);
    }

    public void updateFlashcards(FlashcardModel card) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            db.execSQL("UPDATE " + TABLE_FLASHCARDS +
                    " SET frontText ='" + card.getFrontText() + "', backText ='" +  card.getBackText() + "', frontImageId ='" +  card.getFrontImageId() + "', backImageId ='" + card.getBackImageId() + "', color ='" + card.getFrontColor() + "', pattern ='" + card.getFrontPattern() + "' WHERE _id = "+ card.getItemId());
            db.setTransactionSuccessful();
        } finally {
            if(db.inTransaction()){
                db.endTransaction();
            }
        }
    }

    public ArrayList<FlashcardModel> getFlashcards(int collectionId) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        ArrayList<FlashcardModel> flashcardsList = new ArrayList<>();
        String QUERY = "SELECT * FROM " + TABLE_FLASHCARDS+" WHERE collectionId ='"+collectionId+"' ORDER BY _id DESC";
        Cursor cursor = db.rawQuery(QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    int itemId = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    String frontText = cursor.getString(cursor.getColumnIndex(KEY_FRONT_TEXT));
                    String backText = cursor.getString(cursor.getColumnIndex(KEY_BACK_TEXT));
                    String frontImageId = cursor.getString(cursor.getColumnIndex(KEY_FRONT_IMAGE_ID));
                    String backImageId = cursor.getString(cursor.getColumnIndex(KEY_BACK_IMAGE_ID));
                    String color = cursor.getString(cursor.getColumnIndex(KEY_COLOR));
                    String pattern = cursor.getString(cursor.getColumnIndex(KEY_PATTERN));
                    FlashcardModel itemObject = new FlashcardModel(collectionId, frontText, backText, frontImageId, backImageId, color, pattern);
                    itemObject.setItemId(itemId);
                    flashcardsList.add(itemObject);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "error");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return flashcardsList;
    }

    public void deleteFlashcards(int _id) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            db.execSQL("DELETE from " + TABLE_FLASHCARDS + " WHERE _id =" + _id );
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.d(TAG, "Error while trying to delete  users detail");
        } finally {
            if(db.inTransaction()){
                db.endTransaction();
            }
        }
    }
}
