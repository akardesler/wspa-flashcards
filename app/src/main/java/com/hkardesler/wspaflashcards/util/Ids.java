/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.util;

import com.hkardesler.wspaflashcards.R;

public class Ids {

    public final static String UPDATE_UI = "updateUI";
    public final static String COLLECTION_ID = "collectionId";
    public final static String FLASHCARDS_INDEX = "flashCardsIndex";
    public final static String FUNCTION_KEY = "function";
    public final static int REQUEST_REFRESH_FLASHCARD_ACTIVITY = 0;
    public final static int REQUEST_REFRESH_MAIN_ACTIVITY = 1;
    public final static int REQUEST_STORAGE_PERMISSION = 0;

    public final static String[] PATTERN_NAMES = {"ic_pattern_front_1",
            "ic_pattern_front_2",
            "ic_pattern_front_3",
            "ic_pattern_front_4",
            "ic_pattern_front_5",
            "ic_pattern_front_6",
            "ic_pattern_front_7",
            "ic_pattern_front_8",
            "ic_pattern_front_9",
            "ic_pattern_front_10",
            "ic_pattern_front_11",
            "ic_pattern_front_12",
            "ic_pattern_front_13",
            "ic_pattern_front_14"};

    public final static String[] COLOR_NAMES = {"theme_yellow",
            "theme_green",
            "theme_gray",
            "theme_brown",
            "theme_red",
            "theme_purple"};
}
