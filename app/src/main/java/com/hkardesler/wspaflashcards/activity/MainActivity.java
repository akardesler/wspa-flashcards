/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 *
 */

package com.hkardesler.wspaflashcards.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.adapter.CollectionAdapter;
import com.hkardesler.wspaflashcards.model.CollectionModel;
import com.hkardesler.wspaflashcards.util.Ids;
import java.util.ArrayList;

import database.DBHelper;

public class MainActivity extends AppCompatActivity {

    private ArrayList<CollectionModel> collections;
    private CollectionAdapter adapter;
    private RecyclerView recyclerView;
    private FloatingActionButton btnAddFloat;
    LottieAnimationView animationView;
    FrameLayout rlt_no_item;
    LinearLayout ln_empty_second_row;
    RelativeLayout btnAddItem;
    DBHelper dbHelper;
    GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor("#ff9966"));
        dbHelper = new DBHelper(this);

        collections = dbHelper.getCollections();
        recyclerView = findViewById(R.id.rvCollections);
        btnAddFloat = findViewById(R.id.btnAdd);
        animationView = findViewById(R.id.animation_view);

        rlt_no_item = findViewById(R.id.layout_no_item);
        ln_empty_second_row = rlt_no_item.findViewById(R.id.ln_empty_second_row);
        btnAddItem = rlt_no_item.findViewById(R.id.btnAddItem);

        btnAddItem.post(new Runnable() {
            @Override
            public void run() {
                android.view.ViewGroup.LayoutParams params = btnAddItem
                        .getLayoutParams();
                params.height = btnAddItem.getWidth();
                btnAddItem.setLayoutParams(params);
                ln_empty_second_row.setLayoutParams(params);
            }
        });

        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new CollectionAdapter(this, collections);
        recyclerView.setAdapter(adapter);

        if(collections.size() > 0){
            rlt_no_item.setVisibility(View.INVISIBLE);
            btnAddFloat.setVisibility(View.VISIBLE);
        }else{
            rlt_no_item.setVisibility(View.VISIBLE);
            btnAddFloat.setVisibility(View.GONE);
        }

    }

    public void addCollection(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = (MainActivity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_edit_collection, null);
        AutoCompleteTextView edtCollectionName = dialogView.findViewById(R.id.edtCollectionName);
        Button btnAddDialog = dialogView.findViewById(R.id.btnAdd);
        Button btnDialogCancel = dialogView.findViewById(R.id.btnCancel);

        builder.setView(dialogView);
        builder.create();
        final AlertDialog ad = builder.show();

        btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });

        btnAddDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edtCollectionName.getText().toString().trim().equals("")){
                    CollectionModel collection = new CollectionModel(edtCollectionName.getText().toString());
                    dbHelper.addCollection(edtCollectionName.getText().toString());
                    collections = dbHelper.getCollections();
                    adapter.notifyItemInserted(0);
                    adapter.notifyItemRangeChanged(0, collections.size());
                    adapter.setData(collections);
                    rlt_no_item.setVisibility(View.GONE);
                    btnAddFloat.setVisibility(View.VISIBLE);
                    recyclerView.smoothScrollToPosition(0);

                    ad.dismiss();
                }else{
                    edtCollectionName.setError(getString(R.string.edt_warning));
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Ids.REQUEST_REFRESH_MAIN_ACTIVITY && resultCode == Activity.RESULT_OK && data != null) {
            collections = dbHelper.getCollections();
            boolean updateUI = data.getBooleanExtra(Ids.UPDATE_UI, false);

            if(updateUI)
                adapter.update(collections);

            if(collections.size() > 0){
                rlt_no_item.setVisibility(View.INVISIBLE);
                btnAddFloat.setVisibility(View.VISIBLE);
            }else{
                rlt_no_item.setVisibility(View.VISIBLE);
                btnAddFloat.setVisibility(View.GONE);
            }

        }
    }

    public void createInfoDialog(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = (MainActivity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_info, null);
        ImageView btnClose = dialogView.findViewById(R.id.btnClose);
        TextView txtBitbucketLink = dialogView.findViewById(R.id.txtBitbucketLink);
        txtBitbucketLink.setMovementMethod(LinkMovementMethod.getInstance());

        builder.setView(dialogView);
        builder.create();
        final AlertDialog ad = builder.show();
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        gridLayoutManager.setSpanCount(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? 4 : 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        super.onConfigurationChanged(newConfig);
    }

}