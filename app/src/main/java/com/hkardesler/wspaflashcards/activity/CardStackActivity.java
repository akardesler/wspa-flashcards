/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.adapter.CardStackAdapter;
import com.hkardesler.wspaflashcards.model.FlashcardModel;
import com.hkardesler.wspaflashcards.util.Ids;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.Duration;
import com.yuyakaido.android.cardstackview.RewindAnimationSetting;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeableMethod;

import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import database.DBHelper;

public class CardStackActivity extends AppCompatActivity implements CardStackListener {

    private CardStackView cardsView;
    private CardStackAdapter adapter;
    private CardStackLayoutManager manager;
    private ArrayList<FlashcardModel> flashcards;
    private int collectionId, position;
    private TextView txtPageIndex, txtTitle;
    private ImageView btnRewind;
    private LinearLayout noItemLayout;
    private int disappearredCardPosition;
    private int currentItemPosition;
    private boolean updateUI = false;
    DBHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_card_stack);
        cardsView = findViewById(R.id.card_stack_view);
        txtPageIndex = findViewById(R.id.txtPageIndex);
        txtTitle = findViewById(R.id.txtTitle);
        btnRewind = findViewById(R.id.btnRewind);
        noItemLayout = findViewById(R.id.ln_no_item);

        dbHelper = new DBHelper(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            txtTitle.setText(extras.getString("title"));
            collectionId = extras.getInt(Ids.COLLECTION_ID);
            position = extras.getInt("position");
            flashcards = dbHelper.getFlashcards(collectionId);
        }

        manager = new CardStackLayoutManager(this, this);
        manager.setStackFrom(StackFrom.Right);
        manager.setVisibleCount(5);
        manager.setTranslationInterval(8.0f);
        manager.setScaleInterval(0.95f);
        manager.setSwipeThreshold(0.3f);
        manager.setMaxDegree(20.0f);
        manager.setCanScrollHorizontal(true);
        manager.setCanScrollVertical(false);
        manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual);
        manager.setOverlayInterpolator(new LinearInterpolator());
        manager.setDirections(Arrays.asList(Direction.Left));

        RewindAnimationSetting setting = new RewindAnimationSetting.Builder()
                .setDirection(Direction.Left)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(new LinearInterpolator())
                .build();
        manager.setRewindAnimationSetting(setting);

        cardsView.setLayoutManager(manager);
        adapter = new CardStackAdapter(this, flashcards);
        cardsView.setAdapter(adapter);
        cardsView.scrollToPosition(position);


    }

    @Override
    public void onBackPressed() {
        finishActivity(null);
    }

    public void finishActivity(View v){
        Intent i = new Intent();
        i.putExtra(Ids.UPDATE_UI, updateUI);
        setResult(Activity.RESULT_OK, i);
        finish();
    }

    public void rewindCard(View v){
        noItemLayout.setVisibility(View.GONE);
        cardsView.rewind();
    }

    public void deleteCard(View v){

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.delete_card);
        alert.setMessage(getString(R.string.delete_desc));
        alert.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dbHelper.deleteFlashcards(flashcards.get(currentItemPosition).getItemId());
            flashcards.remove(currentItemPosition);
            adapter.itemRemoved(currentItemPosition, flashcards.size());
            updateUI = true;
            if( currentItemPosition != 0 && currentItemPosition == flashcards.size()){
                rewindCard(null);
            }

            if(flashcards.size() == 0){
                finishActivity(null);
            }
        });
        alert.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
        alert.show();
    }

    public void reloadCardStack(View v){
        noItemLayout.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {

    }

    @Override
    public void onCardSwiped(Direction direction) {
        if(direction == Direction.Left){
            if( disappearredCardPosition == adapter.getItemCount()-1){
                noItemLayout.setVisibility(View.VISIBLE);
            }else if(noItemLayout.getVisibility() == View.VISIBLE){
                noItemLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

    @Override
    public void onCardAppeared(View view, int position) {
        currentItemPosition = position;
        txtPageIndex.setText(String.format("%d/%d", position+1, flashcards.size()));
        if(position == 0){
            btnRewind.setBackground(ContextCompat.getDrawable(CardStackActivity.this, R.drawable.circle_button_disabled));
            btnRewind.setEnabled(false);
        } else{
            btnRewind.setBackground(ContextCompat.getDrawable(CardStackActivity.this, R.drawable.circle_button));
            btnRewind.setEnabled(true);
        }
    }

    @Override
    public void onCardDisappeared(View view, int position) {
        disappearredCardPosition = position;
    }

    public void editFlashcard(View view) {
        Intent i = new Intent(CardStackActivity.this, AddEditFlashcardActivity.class);
        i.putExtra(Ids.COLLECTION_ID, collectionId);
        i.putExtra(Ids.FLASHCARDS_INDEX, currentItemPosition);
        i.putExtra(Ids.FUNCTION_KEY, AddEditFlashcardActivity.FUNCTION_EDIT);
        startActivityForResult(i, Ids.REQUEST_REFRESH_FLASHCARD_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Ids.REQUEST_REFRESH_FLASHCARD_ACTIVITY && resultCode == Activity.RESULT_OK && data != null) {
            updateUI = data.getBooleanExtra(Ids.UPDATE_UI, false);

            if(updateUI) {
                flashcards = dbHelper.getFlashcards(collectionId);
                adapter.setData(flashcards);
                noItemLayout.setVisibility(View.GONE);
            }
        }
    }
}
