/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.hkardesler.wspaflashcards;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.hkardesler.wspaflashcards";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "1.2";
}
