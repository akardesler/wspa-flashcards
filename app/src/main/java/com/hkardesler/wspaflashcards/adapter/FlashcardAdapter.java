/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 *
 */

package com.hkardesler.wspaflashcards.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.activity.CardStackActivity;
import com.hkardesler.wspaflashcards.activity.FlashcardActivity;
import com.hkardesler.wspaflashcards.model.FlashcardModel;
import com.hkardesler.wspaflashcards.util.AppUtils;
import com.hkardesler.wspaflashcards.util.Ids;
import com.hkardesler.wspaflashcards.util.TileDrawable;

import java.io.File;
import java.util.ArrayList;

public class FlashcardAdapter extends RecyclerView.Adapter<FlashcardAdapter.ViewHolder> {

    private final ArrayList<FlashcardModel> mData;
    private final LayoutInflater mInflater;
    private final Context context;
    private final String collectionTitle;
    public FlashcardAdapter(Context context, ArrayList<FlashcardModel> data, String collectionTitle) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
        this.collectionTitle = collectionTitle;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_flashcard, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtContent;
        FrameLayout frameLayout;
        FrameLayout frameLayoutPattern;
        CardView cardView, cardViewImageFront;
        ImageView imageViewFront;

        ViewHolder(View itemView) {
            super(itemView);
            txtContent = itemView.findViewById(R.id.txtContent);
            frameLayout = itemView.findViewById(R.id.frameLayout);
            frameLayoutPattern = itemView.findViewById(R.id.frameLayoutPattern);
            cardView = itemView.findViewById(R.id.card_view);
            cardViewImageFront = itemView.findViewById(R.id.cardview_image);
            imageViewFront = itemView.findViewById(R.id.imageView);
        }

        public void setData(int position){
            FlashcardModel flashcard = mData.get(position);


            if(!flashcard.getFrontImageId().equals("")){
                Uri imageUri;
                if(AppUtils.onlyContainsNumbers(flashcard.getFrontImageId())){
                    imageUri =  ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Long.parseLong(flashcard.getFrontImageId()));
                }else{
                    imageUri = Uri.fromFile(new File(flashcard.getFrontImageId()));
                }

                Glide.with(context)
                        .load(imageUri)
                        .error(R.color.white)
                        .apply(new RequestOptions().centerCrop())
                        .into(imageViewFront);

                txtContent.setVisibility(View.GONE);
                cardViewImageFront.setVisibility(View.VISIBLE);
            }else{
                txtContent.setText(flashcard.getFrontText());
                txtContent.setVisibility(View.VISIBLE);
                cardViewImageFront.setVisibility(View.GONE);
            }

            ColorDrawable colorDrawable = new ColorDrawable(AppUtils.getCardColor(context, flashcard.getFrontColor()));

            Drawable d = AppUtils.getPatternDrawable(context, flashcard.getFrontPattern());
            frameLayoutPattern.setBackground(new TileDrawable(d, Shader.TileMode.REPEAT));
            frameLayout.setBackground(colorDrawable);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CardStackActivity.class);

                    i.putExtra(Ids.COLLECTION_ID, flashcard.getCollectionId());
                    i.putExtra("position", position);
                    i.putExtra("title", collectionTitle);
                    ((FlashcardActivity)context).startActivityForResult(i, Ids.REQUEST_REFRESH_FLASHCARD_ACTIVITY);

                }
            });
        }


    }

    public void update(ArrayList<FlashcardModel> list) {
        mData.clear();
        mData.addAll(list);
       notifyDataSetChanged();
    }

}