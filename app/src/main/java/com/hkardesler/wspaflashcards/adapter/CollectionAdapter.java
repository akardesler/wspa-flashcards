/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 *
 */

package com.hkardesler.wspaflashcards.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.activity.FlashcardActivity;
import com.hkardesler.wspaflashcards.activity.MainActivity;
import com.hkardesler.wspaflashcards.model.CollectionModel;
import com.hkardesler.wspaflashcards.util.Ids;
import com.hkardesler.wspaflashcards.util.TileDrawable;

import java.util.ArrayList;

public class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.ViewHolder> {

    private final ArrayList<CollectionModel> mData;
    private final LayoutInflater mInflater;
    private final Context context;

    public CollectionAdapter(Context context, ArrayList<CollectionModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_collection, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCollectionName, txtCount, txtFlashcard;
        CardView cardView;
        FrameLayout frameLayoutPattern;

        ViewHolder(View itemView) {
            super(itemView);
            txtCollectionName = itemView.findViewById(R.id.txtCollectionName);
            frameLayoutPattern = itemView.findViewById(R.id.frameLayoutPattern);
            txtCount = itemView.findViewById(R.id.txtCount);
            txtFlashcard = itemView.findViewById(R.id.txtFlashcard);
            cardView = itemView.findViewById(R.id.card_view);
        }

        public void setData(int position){
            CollectionModel collection = mData.get(position);

            txtCollectionName.setText(collection.getCollectionName());
            txtCount.setText(String.valueOf(collection.getFlashCardCount()));
            if(collection.getFlashCardCount() > 1)
                txtFlashcard.setText(context.getString(R.string.flashcards));

            Drawable d = ContextCompat.getDrawable(context, R.drawable.ic_pattern_collection);
            frameLayoutPattern.setBackground(new TileDrawable(d, Shader.TileMode.REPEAT));

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, FlashcardActivity.class);

                    i.putExtra("title", collection.getCollectionName());
                    i.putExtra(Ids.COLLECTION_ID, collection.getId());
                    ((MainActivity)context).startActivityForResult(i, Ids.REQUEST_REFRESH_MAIN_ACTIVITY);

                }
            });
        }


    }

    public void update(ArrayList<CollectionModel> list) {
        mData.clear();
        mData.addAll(list);
        notifyDataSetChanged();
    }
    public void setData(ArrayList<CollectionModel> list) {
        mData.clear();
        mData.addAll(list);
    }
}