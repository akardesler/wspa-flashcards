/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.adapter.FlashcardAdapter;
import com.hkardesler.wspaflashcards.model.FlashcardModel;
import com.hkardesler.wspaflashcards.util.Ids;

import java.util.ArrayList;

import database.DBHelper;

public class FlashcardActivity extends AppCompatActivity {

    private ArrayList<FlashcardModel> flashcards;
    private FlashcardAdapter adapter;
    private RecyclerView recyclerView;
    private FloatingActionButton btnAddFloat;
    private FrameLayout layoutNoItem;
    private LinearLayout ln_empty_second_row;
    private RelativeLayout btnAddItem;
    private TextView txtTitle;
    private String title;
    private int collectionId;
    private boolean updateUIMain = false;
    DBHelper dbHelper;
    GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_flashcard);
        dbHelper = new DBHelper(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            title = extras.getString("title");
            collectionId = extras.getInt(Ids.COLLECTION_ID);
            flashcards = dbHelper.getFlashcards(collectionId);
        }

        if(flashcards == null){
            flashcards = new ArrayList<>();
        }

        recyclerView = findViewById(R.id.rvFlashcard);
        txtTitle = findViewById(R.id.txtTitle);
        txtTitle.setText(title);
        btnAddFloat = findViewById(R.id.btnAdd);

        layoutNoItem = findViewById(R.id.layout_no_item);
        ln_empty_second_row = layoutNoItem.findViewById(R.id.ln_empty_second_row);
        btnAddItem = layoutNoItem.findViewById(R.id.btnAddItem);
        btnAddItem.post(new Runnable() {
            @Override
            public void run() {
                android.view.ViewGroup.LayoutParams params = btnAddItem
                        .getLayoutParams();
                params.height =  (int)(btnAddItem.getWidth()*1.5);
                btnAddItem.setLayoutParams(params);
                ln_empty_second_row.setLayoutParams(params);
            }
        });

        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new FlashcardAdapter(this, flashcards, title);
        recyclerView.setAdapter(adapter);

        if(flashcards.size() > 0){
            layoutNoItem.setVisibility(View.INVISIBLE);
            btnAddFloat.setVisibility(View.VISIBLE);
        }else{
            layoutNoItem.setVisibility(View.VISIBLE);
            btnAddFloat.setVisibility(View.GONE);
        }

    }

    public void addFlashcard(View v){
        Intent i = new Intent(FlashcardActivity.this, AddEditFlashcardActivity.class);
        i.putExtra(Ids.COLLECTION_ID, collectionId);
        i.putExtra(Ids.FUNCTION_KEY, AddEditFlashcardActivity.FUNCTION_ADD);
        startActivityForResult(i, Ids.REQUEST_REFRESH_FLASHCARD_ACTIVITY);
    }

    public void deleteCollection(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.delete_collection);
        alert.setMessage(getString(R.string.delete_desc));
        alert.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dbHelper.deleteCollection(collectionId);
            updateUIMain = true;
            finishActivity(null);
        });
        alert.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
        alert.show();

    }

    public void editCollection(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_edit_collection, null);
        TextView txtDialogTitle = dialogView.findViewById(R.id.txtTitle);
        AutoCompleteTextView edtCollectionName = dialogView.findViewById(R.id.edtCollectionName);
        Button btnEditDialog = dialogView.findViewById(R.id.btnAdd);
        Button btnDialogCancel = dialogView.findViewById(R.id.btnCancel);
        btnEditDialog.setText(getString(R.string.save));
        txtDialogTitle.setText(getString(R.string.edit_collection));
        edtCollectionName.setText(title);
        builder.setView(dialogView);
        builder.create();
        final AlertDialog ad = builder.show();

        btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });

        btnEditDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edtCollectionName.getText().toString().trim().equals("")){
                    title = edtCollectionName.getText().toString();
                    dbHelper.updateCollection(collectionId, title);
                    txtTitle.setText(title);
                    updateUIMain = true;
                    ad.dismiss();
                }else{
                    edtCollectionName.setError(getString(R.string.edt_warning));
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Ids.REQUEST_REFRESH_FLASHCARD_ACTIVITY && resultCode == Activity.RESULT_OK && data != null) {
            flashcards = dbHelper.getFlashcards(collectionId);
            boolean updateUI = data.getBooleanExtra(Ids.UPDATE_UI, false);
            if(updateUI) {
                updateUIMain = true;
                adapter.update(flashcards);
            }

            if(flashcards.size() > 0){
                layoutNoItem.setVisibility(View.INVISIBLE);
                btnAddFloat.setVisibility(View.VISIBLE);
            }else{
                layoutNoItem.setVisibility(View.VISIBLE);
                btnAddFloat.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finishActivity(null);
    }

    public void finishActivity(View v){
        Intent i = new Intent();
        i.putExtra(Ids.UPDATE_UI, updateUIMain);
        setResult(Activity.RESULT_OK,i);
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        gridLayoutManager.setSpanCount(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? 6 : 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        super.onConfigurationChanged(newConfig);
    }
}
