# WSPA Flashcards

Enhance your studies with flashcards, easily create study sets. Works fully offline.

A flashcard is a set of cards bearing information, as words or numbers, on either or both sides, used in classroom drills or in private study. Flashcards can bear vocabulary, historical dates, formulas, or any subject matter that can be learned via a question-and-answer format. Flashcards are widely used as a learning drill to aid memorization by way of spaced repetition. With this app, you can create sets, add flashcards and play with them in a study mode. Flashcards can contain text, images, or drawings.

The project is open-source on BitBucket, feel free to contribute.

## Screenshots

![Home](Screenshots/home.jpg)
![Flashcards](Screenshots/flashcards.jpg)
![Flashcards 2](Screenshots/flashcards_2.jpg)
![Card Front](Screenshots/cardfront.jpg)
![Card Back](Screenshots/cardback.jpg)
![Card Back 2](Screenshots/cardback_2.jpg)
![Add Card](Screenshots/addcard.jpg)
![Team](Screenshots/team.jpg)

Pre-requisites
--------------

- Android SDK v30
- Android Build Tools v30.0.3

Team Members
--------------

- Alper Kardesler
- Kaja Wosiek
- Jay Asalalia
- Mayur Vaghani

License
-------

Copyright 2021 The Android Open Source Project, Alper Kardesler.

Licensed to the Apache Software Foundation (ASF) under one or more contributor
license agreements.  See the NOTICE file distributed with this work for
additional information regarding copyright ownership.  The ASF licenses this
file to you under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License.  You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
License for the specific language governing permissions and limitations under
the License.
