/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.util;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.model.CollectionModel;
import com.hkardesler.wspaflashcards.model.FlashcardModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.view.View.VISIBLE;

public class AppUtils {
    private static final String WSPA_FLASHCARDS_APP = "WSPA_FLASHCARDS_APP";

    public static boolean flipItem(Context context, View mCardFrontLayout, View mCardBackLayout,  final boolean mIsBackVisible) {

        AnimatorSet mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.out_animation);
        AnimatorSet mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.in_animation);

            mSetLeftIn.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                    if (!mIsBackVisible) {
                        mCardFrontLayout.setVisibility(View.GONE);
                    } else {
                        mCardBackLayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            if (!mIsBackVisible) {
                mCardBackLayout.setVisibility(VISIBLE);
                mSetRightOut.setTarget(mCardFrontLayout);
                mSetLeftIn.setTarget(mCardBackLayout);
                mSetRightOut.start();
                mSetLeftIn.start();
                return true;

            } else {
                mCardFrontLayout.setVisibility(VISIBLE);
                mSetRightOut.setTarget(mCardBackLayout);
                mSetLeftIn.setTarget(mCardFrontLayout);
                mSetRightOut.start();
                mSetLeftIn.start();
                return false;
            }
    }

    public static void setCameraDistance(Context c, View mCardFrontLayout, View mCardBackLayout) {
        int distance = 6000;
        float scale = (c.getResources().getDisplayMetrics().density) * distance;

        mCardFrontLayout.setCameraDistance(scale);
        mCardBackLayout.setCameraDistance(scale);
    }

    public static Drawable getPatternDrawable(Context context, String patternName){
        Drawable drawablePattern;
        switch (patternName){
            case "ic_pattern_front_2":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_2);
                break;
            case "ic_pattern_front_3":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_3);
                break;
            case "ic_pattern_front_4":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_4);
                break;
            case "ic_pattern_front_5":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_5);
                break;
            case "ic_pattern_front_6":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_6);
                break;
            case "ic_pattern_front_7":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_7);
                break;
            case "ic_pattern_front_8":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_8);
                break;
            case "ic_pattern_front_9":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_9);
                break;
            case "ic_pattern_front_10":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_10);
                break;
            case "ic_pattern_front_11":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_11);
                break;
            case "ic_pattern_front_12":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_12);
                break;
            case "ic_pattern_front_13":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_13);
                break;
            case "ic_pattern_front_14":
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_14);
                break;

            default:
                drawablePattern = ContextCompat.getDrawable(context, R.drawable.ic_pattern_front_1);
                break;
        }
        return drawablePattern;
    }

    public static int getCardColor(Context context, String colorName){
        int color;
        switch (colorName){
            case "theme_yellow":
                color = ContextCompat.getColor(context, R.color.theme_yellow);
                break;
            case "theme_green":
                color = ContextCompat.getColor(context, R.color.theme_green);
                break;
            case "theme_gray":
                color = ContextCompat.getColor(context, R.color.theme_gray);
                break;
            case "theme_brown":
                color = ContextCompat.getColor(context, R.color.theme_brown);
                break;
            case "theme_red":
                color = ContextCompat.getColor(context, R.color.theme_red);
                break;
            case "theme_purple":
                color = ContextCompat.getColor(context, R.color.theme_purple);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + colorName);
        }
        return color;
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static boolean onlyContainsNumbers(String text) {
        try {
            Long.parseLong(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
