/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.model;

import java.util.ArrayList;

public class CollectionModel {

    private int id;
    private String collectionName;
    private long flashCardCount = 0;

    public CollectionModel(String collectionName) {
        this.collectionName = collectionName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getFlashCardCount() {
        return flashCardCount;
    }

    public void setFlashCardCount(long flashCardCount) {
        this.flashCardCount = flashCardCount;
    }
}
