/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.model.FlashcardModel;
import com.hkardesler.wspaflashcards.util.AppUtils;
import com.hkardesler.wspaflashcards.util.Ids;
import com.hkardesler.wspaflashcards.util.TileDrawable;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import database.DBHelper;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class AddEditFlashcardActivity extends AppCompatActivity {

    private boolean mIsBackVisible = false;
    private View mCardFrontLayout, mCardBackLayout;
    private EditText edtCardFrontContent, edtCardBackContent;
    private TextView txtTaptoFlipFront, txtTaptoFlipBack, txtOrFront, txtOrBack;
    private FrameLayout mFrontFrameLayout, mPatternLayoutBack, mPatternLayoutFront;
    private ImageView img_circle, imageViewFront, imageViewBack, btnRemoveImageFront, btnChangeImageFront, btnRemoveImageBack, btnChangeImageBack;
    private CardView cardViewImageFront, cardViewImageBack;
    RelativeLayout rltContentFront, rltContentBack;
    Button btnAddImageFront, btnAddImageBack;
    private int collectionId, flashcardsIndex;
    private String selectedCardColor;
    private String frontPattern;
    boolean isFlipping = false;
    public static int FUNCTION_ADD = 0;
    public static int FUNCTION_EDIT = 1;
    private int function = 0;
    FlashcardModel flashCardItem;
    private final int PICK_IMAGE_GALLERY_FOR_FRONT = 1;
    private final int PICK_IMAGE_GALLERY_FOR_BACK = 2;
    private String frontImageId = "";
    private String backImageId = "";
    private String btnImageTag = "front";
    DBHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_edit_flashcard);
        dbHelper = new DBHelper(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            collectionId = extras.getInt(Ids.COLLECTION_ID, 0);
            function = extras.getInt(Ids.FUNCTION_KEY, 0);
            flashcardsIndex = extras.getInt(Ids.FLASHCARDS_INDEX, 0);
        }

        initViews();
        txtTaptoFlipFront.setVisibility(GONE);
        txtTaptoFlipBack.setVisibility(GONE);
        btnRemoveImageFront.setVisibility(VISIBLE);
        btnRemoveImageBack.setVisibility(VISIBLE);
        btnChangeImageFront.setVisibility(VISIBLE);
        btnChangeImageBack.setVisibility(VISIBLE);

        ArrayList<FlashcardModel> flashcards = dbHelper.getFlashcards(collectionId);

        if(function == FUNCTION_EDIT){

            flashCardItem = flashcards.get(flashcardsIndex);
            if(!flashCardItem.getFrontImageId().equals("")){
                frontImageId = flashCardItem.getFrontImageId();
                Uri imageUri;
                if(AppUtils.onlyContainsNumbers(frontImageId)){
                    imageUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Long.parseLong(frontImageId));
                }else{
                    imageUri = Uri.fromFile(new File(frontImageId));
                }

                Glide.with(this)
                        .load(imageUri)
                        .error(R.color.white)
                        .apply(new RequestOptions().centerCrop())
                        .into(imageViewFront);

               cardViewImageFront.setVisibility(VISIBLE);
            }else{
                cardViewImageFront.setVisibility(GONE);
                txtOrFront.setVisibility(GONE);
                btnAddImageFront.setVisibility(GONE);
                rltContentFront.setVisibility(VISIBLE);
                edtCardFrontContent.setText(flashCardItem.getFrontText());
                edtCardFrontContent.setVisibility(VISIBLE);
            }

            if(!flashCardItem.getBackImageId().equals("")){
                backImageId = flashCardItem.getBackImageId();
                Uri imageUri;
                if(AppUtils.onlyContainsNumbers(backImageId)){
                    imageUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Long.parseLong(backImageId));
                }else{
                    imageUri = Uri.fromFile(new File(backImageId));
                }

                Glide.with(this)
                        .load(imageUri)
                        .error(R.color.white)
                        .apply(new RequestOptions().centerCrop())
                        .into(imageViewBack);
                cardViewImageBack.setVisibility(VISIBLE);

            }else{
                cardViewImageBack.setVisibility(GONE);
                txtOrBack.setVisibility(GONE);
                btnAddImageBack.setVisibility(GONE);
                rltContentBack.setVisibility(VISIBLE);
                edtCardBackContent.setText(flashCardItem.getBackText());
                edtCardBackContent.setVisibility(VISIBLE);
            }
        }else{
            rltContentFront.setVisibility(VISIBLE);
            rltContentBack.setVisibility(VISIBLE);
            txtOrFront.setVisibility(VISIBLE);
            txtOrBack.setVisibility(VISIBLE);
            btnAddImageFront.setVisibility(VISIBLE);
            btnAddImageBack.setVisibility(VISIBLE);
        }

        edtCardFrontContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0){
                    txtOrFront.setVisibility(GONE);
                    btnAddImageFront.setVisibility(GONE);
                }else{
                    txtOrFront.setVisibility(VISIBLE);
                    btnAddImageFront.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtCardBackContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0){
                    txtOrBack.setVisibility(GONE);
                    btnAddImageBack.setVisibility(GONE);
                }else{
                    txtOrBack.setVisibility(VISIBLE);
                    btnAddImageBack.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        setCardColor();
        frontPattern = createFrontPattern();

        Drawable dFront = AppUtils.getPatternDrawable(this, frontPattern);
        mPatternLayoutFront.setBackground(new TileDrawable(dFront, Shader.TileMode.REPEAT));

        Drawable dBack = ContextCompat.getDrawable(this, R.drawable.ic_pattern_card_back);
        mPatternLayoutBack.setBackground(new TileDrawable(dBack, Shader.TileMode.REPEAT));

        AppUtils.setCameraDistance(this, mCardFrontLayout,mCardBackLayout);
    }

    private void initViews(){
        mCardBackLayout = findViewById(R.id.card_back);
        mCardFrontLayout = findViewById(R.id.card_front);

        edtCardFrontContent = mCardFrontLayout.findViewById(R.id.txtContent);
        edtCardBackContent = mCardBackLayout.findViewById(R.id.txtContent);
        mFrontFrameLayout = mCardFrontLayout.findViewById(R.id.frameLayout);
        mPatternLayoutFront = mCardFrontLayout.findViewById(R.id.frameLayoutPattern);
        mPatternLayoutBack = mCardBackLayout.findViewById(R.id.frameLayoutPattern);
        txtTaptoFlipFront = mCardFrontLayout.findViewById(R.id.txtTaptoFlip);
        txtTaptoFlipBack = mCardBackLayout.findViewById(R.id.txtTaptoFlip);

        cardViewImageFront = mCardFrontLayout.findViewById(R.id.cardview_image);
        imageViewFront = mCardFrontLayout.findViewById(R.id.imageView);
        rltContentFront = mCardFrontLayout.findViewById(R.id.rlt_content);
        btnAddImageFront = mCardFrontLayout.findViewById(R.id.btnAddImage);
        txtOrFront = mCardFrontLayout.findViewById(R.id.txtOr);

        cardViewImageBack = mCardBackLayout.findViewById(R.id.cardview_image);
        imageViewBack = mCardBackLayout.findViewById(R.id.imageView);
        rltContentBack = mCardBackLayout.findViewById(R.id.rlt_content);
        btnAddImageBack = mCardBackLayout.findViewById(R.id.btnAddImage);
        txtOrBack = mCardBackLayout.findViewById(R.id.txtOr);

        btnRemoveImageFront = mCardFrontLayout.findViewById(R.id.btnRemoveImage);
        btnChangeImageFront = mCardFrontLayout.findViewById(R.id.btnChangeImage);
        btnRemoveImageBack= mCardBackLayout.findViewById(R.id.btnRemoveImage);
        btnChangeImageBack = mCardBackLayout.findViewById(R.id.btnChangeImage);
    }

    private void setCardColor(){
        if(function == FUNCTION_ADD){
            selectedCardColor = Ids.COLOR_NAMES[new Random().nextInt(Ids.COLOR_NAMES.length)];
        }else{
            selectedCardColor = flashCardItem.getFrontColor();

        }

        switch (selectedCardColor){
            case "theme_yellow":
                img_circle =  findViewById(R.id.circle_yellow);
                break;
            case "theme_green":
                img_circle =  findViewById(R.id.circle_green);
                break;
            case "theme_gray":
                img_circle =  findViewById(R.id.circle_gray);
                break;
            case "theme_brown":
                img_circle =  findViewById(R.id.circle_brown);
                break;
            case "theme_red":
                img_circle =  findViewById(R.id.circle_red);
                break;
            case "theme_purple":
                img_circle =  findViewById(R.id.circle_purple);
        }

        img_circle.setBackground(ContextCompat.getDrawable(AddEditFlashcardActivity.this, R.drawable.circle_border));
        ColorDrawable colorDrawable = new ColorDrawable(AppUtils.getCardColor(this, selectedCardColor));
        mFrontFrameLayout.setBackground(colorDrawable);
    }

    private String createFrontPattern(){
        String pattern;
        if(function == FUNCTION_ADD){
            pattern = Ids.PATTERN_NAMES[new Random().nextInt(Ids.PATTERN_NAMES.length)];
        }else{
            pattern = flashCardItem.getFrontPattern();
        }

        return pattern;
    }

    public void clickDone(View v){
        if((!edtCardFrontContent.getText().toString().trim().equals("") || !frontImageId.equals("")) &&
                (!edtCardBackContent.getText().toString().trim().equals("") || !backImageId.equals(""))){

            FlashcardModel card = new FlashcardModel(collectionId, edtCardFrontContent.getText().toString(),
                    edtCardBackContent.getText().toString(), frontImageId, backImageId, selectedCardColor, frontPattern);

            if(function == FUNCTION_ADD){
                dbHelper.addFlashcards(card);
            }else{
                card.setItemId(flashCardItem.getItemId());
                dbHelper.updateFlashcards(card);
            }

            Intent i = new Intent();
            i.putExtra(Ids.UPDATE_UI, true);
            setResult(Activity.RESULT_OK, i);
            finish();

        }else{
            Snackbar.make(findViewById(android.R.id.content),getString(R.string.card_edt_warning), Snackbar.LENGTH_LONG).show();
            mIsBackVisible = AppUtils.flipItem(this, mCardFrontLayout, mCardBackLayout, mIsBackVisible);
        }
    }

    public void colorClick(View view) {

        img_circle.setBackground(null);
        img_circle = (ImageView)view;
        img_circle.setBackground(ContextCompat.getDrawable(AddEditFlashcardActivity.this, R.drawable.circle_border));

        if (view.getId() == R.id.circle_yellow) {
            selectedCardColor = Ids.COLOR_NAMES[0];
        } else if (view.getId() == R.id.circle_green) {
            selectedCardColor = Ids.COLOR_NAMES[1];
        } else if (view.getId() == R.id.circle_gray) {
            selectedCardColor = Ids.COLOR_NAMES[2];
        }else if (view.getId() == R.id.circle_brown) {
            selectedCardColor = Ids.COLOR_NAMES[3];
        }else if (view.getId() == R.id.circle_red) {
            selectedCardColor = Ids.COLOR_NAMES[4];
        }else {
            selectedCardColor = Ids.COLOR_NAMES[5];
        }

        ColorDrawable colorDrawable = new ColorDrawable(AppUtils.getCardColor(this, selectedCardColor));
        mFrontFrameLayout.setBackground(colorDrawable);

    }

    private void animationHandler(){
        isFlipping = true;
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            isFlipping = false;

        }, 800);
    }

    public void flipCard(View v){
        if(!isFlipping){
            animationHandler();
            mIsBackVisible = AppUtils.flipItem(this, mCardFrontLayout, mCardBackLayout, mIsBackVisible);
        }
    }

    public void showGallery() {
        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            loadGallery();
        } else {
            requestPermissionsSafely(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Ids.REQUEST_STORAGE_PERMISSION);
        }
    }

    private void loadGallery() {
        Intent choose = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        if(btnImageTag.equals("front")){
            startActivityForResult(choose, PICK_IMAGE_GALLERY_FOR_FRONT);
        }else{
            startActivityForResult(choose, PICK_IMAGE_GALLERY_FOR_BACK);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Ids.REQUEST_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadGallery();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_GALLERY_FOR_FRONT) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();

                if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    frontImageId = String.valueOf(ContentUris.parseId(selectedImage));
                    selectedImage = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Long.parseLong(frontImageId));
                }else{
                    frontImageId = AppUtils.getRealPathFromUri(AddEditFlashcardActivity.this, selectedImage);
                }

                Glide.with(AddEditFlashcardActivity.this)
                        .load(selectedImage)
                        .error(R.color.white)
                        .apply(new RequestOptions().centerCrop())
                        .into(imageViewFront);

                rltContentFront.setVisibility(GONE);
                cardViewImageFront.setVisibility(VISIBLE);
            }
        }else if(requestCode == PICK_IMAGE_GALLERY_FOR_BACK){
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();

                if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    backImageId = String.valueOf(ContentUris.parseId(selectedImage));
                    selectedImage = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Long.parseLong(backImageId));
                }else{
                    backImageId = AppUtils.getRealPathFromUri(AddEditFlashcardActivity.this, selectedImage);
                }

                Glide.with(AddEditFlashcardActivity.this)
                        .load(selectedImage)
                        .error(R.color.white)
                        .apply(new RequestOptions().centerCrop())
                        .into(imageViewBack);

                rltContentBack.setVisibility(GONE);
                cardViewImageBack.setVisibility(VISIBLE);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void addImageClick(View v){
        btnImageTag = v.getTag().toString();
        showGallery();
    }

    public void removeImageClick(View v){

        if(v.getTag().toString().equals("front")){
            imageViewFront.setImageDrawable(null);
            frontImageId = "";
            rltContentFront.setVisibility(VISIBLE);
            cardViewImageFront.setVisibility(GONE);
            txtOrFront.setVisibility(VISIBLE);
            btnAddImageFront.setVisibility(VISIBLE);
        }else{
            imageViewBack.setImageDrawable(null);
            backImageId = "";
            rltContentBack.setVisibility(VISIBLE);
            cardViewImageBack.setVisibility(GONE);
            txtOrBack.setVisibility(VISIBLE);
            btnAddImageBack.setVisibility(VISIBLE);
        }

    }

    public void finishActivity(View v){
        finish();
    }

}
