/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.model;

public class FlashcardModel {

    private int itemId, collectionId;
    private String frontText, backText, frontImageId, backImageId, frontColor, frontPattern;

    public FlashcardModel(int collectionId, String frontText, String backText, String frontImageId, String backImageId, String frontColor, String frontPattern) {
        this.collectionId = collectionId;
        this.frontText = frontText;
        this.backText = backText;
        this.frontColor = frontColor;
        this.frontPattern = frontPattern;
        this.frontImageId = frontImageId;
        this.backImageId = backImageId;
    }

    public String getFrontText() {
        return frontText;
    }

    public void setFrontText(String frontText) {
        this.frontText = frontText;
    }

    public String getBackText() {
        return backText;
    }

    public void setBackText(String backText) {
        this.backText = backText;
    }

    public String getFrontColor() {
        return frontColor;
    }

    public void setFrontColor(String frontColor) {
        this.frontColor = frontColor;
    }

    public String getFrontPattern() {
        return frontPattern;
    }

    public void setFrontPattern(String frontPattern) {
        this.frontPattern = frontPattern;
    }

    public String getFrontImageId() {
        return frontImageId;
    }

    public void setFrontImageId(String frontImageId) {
        this.frontImageId = frontImageId;
    }

    public String getBackImageId() {
        return backImageId;
    }

    public void setBackImageId(String backImageId) {
        this.backImageId = backImageId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(int collectionId) {
        this.collectionId = collectionId;
    }
}
