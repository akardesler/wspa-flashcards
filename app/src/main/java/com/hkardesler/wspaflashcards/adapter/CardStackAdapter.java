/*
 * Created by Alper Kardesler.
 * Copyright (c) 2021. All Rights Reserved.
 */

package com.hkardesler.wspaflashcards.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hkardesler.wspaflashcards.R;
import com.hkardesler.wspaflashcards.model.FlashcardModel;
import com.hkardesler.wspaflashcards.util.AppUtils;
import com.hkardesler.wspaflashcards.util.TileDrawable;

import java.io.File;
import java.util.ArrayList;

public class CardStackAdapter extends RecyclerView.Adapter<CardStackAdapter.ViewHolder> {

    private final ArrayList<FlashcardModel> mData;
    private final LayoutInflater mInflater;
    private final Context context;

    public CardStackAdapter(Context context, ArrayList<FlashcardModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_swipeable_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        View cardFront, cardBack;
        EditText edtContentFront, edtContentBack;
        FrameLayout frameLayoutFront,layoutPatternFront, layoutPatternBack, flipClickLayout;
        ImageView imageViewFront, imageViewBack;
        private final CardView cardViewImageFront;
        private final CardView cardViewImageBack;
        RelativeLayout rltContentFront, rltContentBack;
        Button btnAddImageFront, btnAddImageBack;
        boolean mIsBackVisible = false;
        Handler handler;
        Runnable runnable;
        boolean isFlipping = false;

        ViewHolder(View itemView) {
            super(itemView);

            flipClickLayout = itemView.findViewById(R.id.flipClickLayout);
            cardFront = itemView.findViewById(R.id.card_front);
            cardBack = itemView.findViewById(R.id.card_back);

            layoutPatternFront = cardFront.findViewById(R.id.frameLayoutPattern);
            layoutPatternBack = cardBack.findViewById(R.id.frameLayoutPattern);
            edtContentFront = cardFront.findViewById(R.id.txtContent);
            frameLayoutFront = cardFront.findViewById(R.id.frameLayout);
            edtContentBack = cardBack.findViewById(R.id.txtContent);
            rltContentFront = cardFront.findViewById(R.id.rlt_content);
            btnAddImageFront = cardFront.findViewById(R.id.btnAddImage);
            imageViewFront = cardFront.findViewById(R.id.imageView);
            rltContentBack = cardBack.findViewById(R.id.rlt_content);
            btnAddImageBack = cardBack.findViewById(R.id.btnAddImage);
            imageViewBack = cardBack.findViewById(R.id.imageView);
            cardViewImageFront = cardFront.findViewById(R.id.cardview_image);
            cardViewImageBack = cardBack.findViewById(R.id.cardview_image);

            edtContentFront.setEnabled(false);
            edtContentBack.setEnabled(false);

            AppUtils.setCameraDistance(context, cardFront,cardBack);

            flipClickLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        if(!isFlipping){
                            animationHandler();
                            mIsBackVisible = AppUtils.flipItem(context, cardFront, cardBack, mIsBackVisible);
                        }
                }
            });

        }

        private void animationHandler(){
            isFlipping = true;
            handler = new Handler(Looper.getMainLooper());
            runnable = () -> {
                isFlipping = false;
            };
            handler.postDelayed(runnable, 800);
        }

        public void setData(int position) {
            FlashcardModel flashcard = mData.get(position);

            if(!flashcard.getFrontImageId().equals("")){
                Uri imageUri;
                if(AppUtils.onlyContainsNumbers(flashcard.getFrontImageId())){
                     imageUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Long.parseLong(flashcard.getFrontImageId()));
                }else{
                    imageUri = Uri.fromFile(new File(flashcard.getFrontImageId()));
                }

                Glide.with(context)
                        .load(imageUri)
                        .error(R.color.white)
                        .apply(new RequestOptions().centerCrop())
                        .into(imageViewFront);

                rltContentFront.setVisibility(View.GONE);
                cardViewImageFront.setVisibility(View.VISIBLE);
            }else{
                edtContentFront.setText(flashcard.getFrontText());
                rltContentFront.setVisibility(View.VISIBLE);
                cardViewImageFront.setVisibility(View.GONE);
            }

            if(!flashcard.getBackImageId().equals("")){
                Uri imageUri;
                if(AppUtils.onlyContainsNumbers(flashcard.getBackImageId())){
                    imageUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Long.parseLong(flashcard.getBackImageId()));
                }else{
                    imageUri = Uri.fromFile(new File(flashcard.getBackImageId()));
                }

                Glide.with(context)
                        .load(imageUri)
                        .error(R.color.white)
                        .apply(new RequestOptions().centerCrop())
                        .into(imageViewBack);

                rltContentBack.setVisibility(View.GONE);
                cardViewImageBack.setVisibility(View.VISIBLE);
            }else{
                edtContentBack.setText(flashcard.getBackText());
                rltContentBack.setVisibility(View.VISIBLE);
                cardViewImageBack.setVisibility(View.GONE);
            }

            ColorDrawable colorDrawable = new ColorDrawable(AppUtils.getCardColor(context, flashcard.getFrontColor()));
            Drawable drawableFront = AppUtils.getPatternDrawable(context, flashcard.getFrontPattern());
            Drawable drawableBack = ContextCompat.getDrawable(context, R.drawable.ic_pattern_card_back);
            TileDrawable tileFront = new TileDrawable(drawableFront, Shader.TileMode.REPEAT);
            TileDrawable tileBack = new TileDrawable(drawableBack, Shader.TileMode.REPEAT);

            layoutPatternFront.setBackground(tileFront);
            layoutPatternBack.setBackground(tileBack);
            frameLayoutFront.setBackground(colorDrawable);

        }

    }

    public void itemRemoved(int index, int size) {
        notifyItemRemoved(index);
        notifyItemRangeChanged(index, size);
    }

    public void setData(ArrayList<FlashcardModel> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

}

